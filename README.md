# Dockerized Training of Glove Embeddings

# Purpose
Dockerized training of german glove embeddings using latest wikipedia corpus or other plain, preprocessed text files.

# Approach 
1. Download latest articles from german wiki
2. Convert them with gensim into text, single words seperated by space, without punctuation
3. Train glove and save the resulting embedding files in the mounted OUTPUTDIR

# Usage on AWS 
use AMI: Ubuntu Server 16.04 LTS (HVM), SSD Volume Type - ami-759bc50a
with SSD EBS attatched (for German wiki 500GB is needed)

1. `git clone https://gitlab.com/deepset-ai/open-source/glove-embeddings-de.git`
2. `cd glove-embeddings-de`
3. `./install_docker.sh`  
4. either pull the image with `sudo docker pull registry.gitlab.com/deepset-ai/open-source/glove-embeddings-de` 
or build it yourself with `sudo docker build -t glove-de .` and adjust image: glove-de in "docker-compose-wiki.yml"
5. Adjust the mounted volumes in "docker-compose-wiki.yml" to some empty folder on your host
6. Adjust config via environment variables in "docker-compose-wiki.yml"
7. `sudo docker-compose -f docker-compose-wiki.yml up`
8. After termination you will find your embeddings in the directory that you mounted in 5.

Training time: ~ 15h on an EC2 Instance of type m5.2xlarge

Download of Wikipedia dump sometimes (observed 1 out of 12 times) fails, a restart is needed.

# Config
You can configure the glove training via the env variables in `docker-compose-wiki.yml`.  
