FROM ubuntu:16.04
## Set a default user. Available via runtime flag `--user docker`
RUN useradd docker \
	&& mkdir /home/docker \
	&& chown docker:docker /home/docker \
	&& addgroup docker staff

RUN apt-get update -qq \
 && apt-get install --no-install-recommends -y \
    # install essentials
    build-essential \
    g++ \
    git \
    openssh-client \
    # install python 3
    python3 \
    python3-dev \
    python3-pip \
    python3-venv \
    python3-setuptools \
    # requirements for numpy
    libopenblas-base \
    wget \
    locales \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt-get update \
    	&& apt-get install -y libx11-dev libfreetype6-dev libpng-dev pkg-config liblapack3 python-dev

RUN apt-get update \
    	&& apt-get install -y libpq-dev python-dev python3-dev libxslt1-dev libldap2-dev libsasl2-dev libffi-dev  \
     build-essential libssl-dev libffi-dev \
     libxslt1-dev zlib1g-dev libhdf5-dev libhdf5-serial-dev python-pil

#PYTHON REQUIREMENTS
RUN pip3 install pip --upgrade
RUN pip3 install wheel
ADD requirements.txt /home/docker/code/requirements.txt
RUN pip3 install -r /home/docker/code/requirements.txt

#trigger start script
ADD train_glove.sh /home/docker/code/train_glove.sh
ADD Makefile /home/docker/code/Makefile
ADD process_wikipedia.py /home/docker/code/process_wikipedia.py
ADD src /home/docker/code/src
CMD /bin/bash -c "cd /home/docker/code/ && ./train_glove.sh"

